import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_HOST || '127.0.0.1',
      port: 5433,
      username: 'postgres',
      password: process.env.POSTGRES_PASSWORD || 'mysecretpassword',
      database: 'my_database',
      entities: ["dist/**/*.entity.js"],
      migrations: [
          "dist/database/migrations/*{.ts,.js}"
      ],
      schema: 'public',
      // TODO: NO synchronize en Staging/UAT/PROD
      // synchronize: true
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }