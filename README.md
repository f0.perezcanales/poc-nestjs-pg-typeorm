# 1 -  NestJS, TypeORM and PostgreSQL — full example development and project setup working with database migrations.

https://medium.com/@gausmann.simon/nestjs-typeorm-and-postgresql-full-example-development-and-project-setup-working-with-database-c1a2b1b11b8f


### 1.1 - Run app and access to URL first endpoint

```
npm run start:dev
http://localhost:3010/
```

### 1.2. Setting up a local PostgreSQL database instance — with docker automation!

````
npm run start:dev:db 
````

### 1.3

````
npm install --save @nestjs/typeorm typeorm pg

nest start --watch

````


### 1.4 NestJs migrations with TypeORM 0.3 (latest) - How to set it up from scratch using Datasource

 https://www.youtube.com/watch?v=Ty_Zs2w3KYo&t=1244s

````
cd src

nest g resource users

npm install --save @nestjs/mapped-types

````


### 1.5 

```
npm run migration:generate -- database/migrations/CreateUserInfoTable

npm run migration:run
```

# Construir un Pipeline en Gitlab

### Inject secrets Service Account

npm install --save gcloud