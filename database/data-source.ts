import {DataSourceOptions, DataSource} from 'typeorm'
require('dotenv').config()

export const dataSourceOptions: DataSourceOptions = {
    type: 'postgres',
    host: process.env.POSTGRES_HOST || '127.0.0.1',
    port: parseInt(process.env.POSTGRES_PORT) || 5433,
    username: 'postgres',
    password: process.env.POSTGRES_PASSWORD || 'mysecretpassword',
    database: process.env.POSTGRES_DATABASE || 'my_database',
    entities: ["dist/**/*.entity.js"],
    migrations: [
        "dist/database/migrations/*{.ts,.js}"
    ],
    schema: 'public',
}

const datasource = new DataSource(dataSourceOptions)
export default datasource