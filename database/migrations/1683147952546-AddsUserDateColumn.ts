import { MigrationInterface, QueryRunner } from "typeorm";

export class AddsUserDateColumn1683147952546 implements MigrationInterface {
    name = 'AddsUserDateColumn1683147952546'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "item" ADD "date" character varying(30) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "item" DROP COLUMN "date"`);
    }

}
